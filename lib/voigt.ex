defmodule Voigt do

  require Logger

  def gains(signals) do
    signals
    |> early_sort()
    |> Enum.reduce(%{ positions: %{}, gains: %{} }, &simulate_action/2)
    |> Map.get(:gains)
  end

  defp early_sort(signals) do
    Enum.sort_by(signals, &Map.get(&1, :timestamp), &before?/2)
  end

  defp before?(time1, time2) do
    NaiveDateTime.compare(time1, time2) == :lt
  end

  # when holding, close a position, then open the exact same position, but record the gain
  defp simulate_action(signal = %{ type: "hold", ticker: ticker }, state) do
    state
    |> pop_in([:positions, ticker])
    |> handle_hold(signal)
  end

  defp simulate_action(signal = %{ ticker: ticker }, state) do
    state
    |> pop_in([:positions, ticker])
    |> open_or_close_position(signal)
  end

  defp handle_hold({nil, state}, signal) do
    Logger.error("Unable to hold signal: #{inspect signal}")
    state
  end

  defp handle_hold({{_, type} = position, state}, signal) do
    state
    |> close_position(signal, position)
    |> open_position(signal, type)
  end

  defp open_or_close_position({nil, state}, signal) do
    open_position(state, signal)
  end

  defp open_or_close_position({position, state}, signal) do
    close_position(state, signal, position)
  end

  defp open_position(state, %{ ticker: ticker } = signal) do
    state
    |> put_in([:positions, ticker], opening_position(signal))
  end

  defp open_position(state, %{ type: "hold" } = signal, old_type) do
    state
    |> put_in([:positions, signal.ticker], { signal.price, old_type })
  end

  defp opening_position(%{ type: "sell" } = signal) do
    { signal.price, :short }
  end

  defp opening_position(%{ type: "buy" } = signal) do
    { signal.price, :long }
  end

  defp close_position(state, signal, position) do
    state
    |> put_in([:gains, trade_signature(signal)], calculate_gain(signal, position))
  end

  defp trade_signature(%{ ticker: ticker, timestamp: timestamp }) do
    "#{ticker} #{timestamp}"
  end

  defp calculate_gain(%{ price: sell_price }, { buy_price, :long }) do
    ( sell_price - buy_price ) / buy_price
  end

  defp calculate_gain(%{ price: buy_price }, { sell_price, :short }) do
    ( sell_price - buy_price ) / sell_price
  end
end
