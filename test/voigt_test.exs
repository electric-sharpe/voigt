defmodule VoigtTest do
  @moduledoc """
  Unit tests for the Voigt calculator
  """

  use ExUnit.Case, async: true
  @moduletag capture_log: true

  describe "gains/1" do
    test "works with regular signals" do
      signals = [
        %{ ticker: "QRTEA", type: "buy", price: 2, timestamp: ~N[2020-02-27 15:30:09]},
        %{ ticker: "QRTEA", type: "sell", price: 1, timestamp: ~N[2020-02-28 15:30:09]},
        %{ ticker: "QRTEA", type: "sell", price: 1, timestamp: ~N[2020-02-28 15:30:09]},
        %{ ticker: "QRTEA", type: "buy", price: 2, timestamp: ~N[2020-02-29 15:30:09]},
        %{ ticker: "AAL", type: "buy", price: 2, timestamp: ~N[2020-02-29 15:30:09]},
        %{ ticker: "AAL", type: "hold", price: 3, timestamp: ~N[2020-03-01 15:30:09]},
        %{ ticker: "AAL", type: "sell", price: 4, timestamp: ~N[2020-03-02 15:30:09]},
      ]

      assert Voigt.gains(signals) == %{
        "QRTEA 2020-02-28 15:30:09" => -0.5,
        "QRTEA 2020-02-29 15:30:09" => -1.0,
        "AAL 2020-03-01 15:30:09" => 0.5,
        "AAL 2020-03-02 15:30:09" => 0.3333333333333333
      }
    end
  end
end
